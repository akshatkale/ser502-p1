#Project-1 by Team-2
[Survey of Interesting languages video](https://www.youtube.com/watch?v=enIdfv89etI&feature=youtu.be)

1. Install XAMPP PHP development environment and start the Apache web server. If you have skype installed, make sure you configure Skype to use port 81 instead of 80 by going to Skype > tools.

2. After installing XAMPP, go to a folder named htdocs inside XAMPP folder in C drive and copy the programs into that folder.  

3. Open browser and type---> localhost/filename.php to run the file in the browser.