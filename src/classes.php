
<!--Program to demonstrate the use of classes and Object oriented programming in PHP. This is not in the video due to time constraint on 10 minutes.-->
<?php
class Person
{
    public $firstName;
    public $lastName;
 
    public function __construct($firstName, $lastName = '') { // optional second argument
        $this->firstName = $firstName;
        $this->lastName  = $lastName;
    }
 
    public function greet() {
        return 'Hello, my name is ' . $this->firstName .
               (($this->lastName != '') ? (' ' . $this->lastName) : '') . '.';
    }
 
}

 
$he    = new Person('Jim', 'Smith');
$other = new Person('Danny');
 
echo $he->greet(); // prints "Hello, my name is John Smith."
echo '<br />';
 
 
echo $other->greet(); // prints "Hello, my name is Danny."
echo '<br />';
?>
