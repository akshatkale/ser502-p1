
<!--Program to demonstrate the use of superglobals/globalvariables in PHP-->

<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Name: <input type="text" name="fname">
  <input type="submit">
</form>

<?php

function name()
	{
		if ($_SERVER["REQUEST_METHOD"] == "POST") {   
			$name = $_POST['fname'];                       // collect value of input field
			if (empty($name)) {
				$noname = "Name is empty";
				return $noname;
			} else {
				return $name;
			}
		}
	}
	
echo name();
?>
