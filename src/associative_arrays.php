<?php

// program to demonstrate associative arrays.

$rollnumber = array("121" => "Alice", "122" => "Bob", "123" => "Chad");
foreach($rollnumber as $x => $x_value)
	{
		echo "Key = " . $x . " and Value = " . $rollnumber["$x"] . "<br />" . "<br />";

	}
?>

<!--Program to demonstrate the use of superglobals/globalvariables in PHP-->
<!--In PHP, superglobals are associative arrays which are made by the server. The key of the array are indexes.--> 

<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Name: <input type="text" name="fname">
  <input type="submit">
</form>


<?php
function name()
	{
		if ($_SERVER["REQUEST_METHOD"] == "POST") {   
			$name = $_POST['fname'];                       // collect value of input field
			if (empty($name)) {
				$noname = "Name is empty";
				return $noname;
			} else {
				return $name;
			}
		}
	}
	
echo name();
?>